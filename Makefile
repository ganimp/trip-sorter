#############################
# Docker machine states
#############################

COMPOSE_ENV=COMPOSE_HTTP_TIMEOUT=240
COMPOSE:=env $(COMPOSE_ENV) docker-compose -f ./docker/docker-compose.yml

up:
	$(COMPOSE) up -d

down:
	$(COMPOSE) down

start:
	$(COMPOSE) start

stop:
	$(COMPOSE) stop

state:
	$(COMPOSE) ps

start-app: up install

rebuild: stop
	$(COMPOSE) pull
	$(COMPOSE) rm --force app
	$(COMPOSE) build --no-cache
	$(COMPOSE) up -d --force-recreate

build:
	$(COMPOSE) build --no-cache

install: install-deps

install-deps: install-composer install-phpdoc
	php ./bin/composer install --ignore-platform-reqs

install-phpdoc:
	curl -so ./bin/phpdoc https://phpdoc.org/phpDocumentor.phar
	chmod a+x ./bin/phpdoc

install-composer:
	@if [ ! -d ./bin ]; then mkdir bin; fi
	@if [ ! -f ./bin/composer ]; then curl -s https://getcomposer.org/installer | php -- --install-dir=./bin --filename=composer; fi

enter-container:
	$(COMPOSE) run --rm --no-deps -e USER=test php bash

test: install
	$(COMPOSE) run --rm --no-deps -e USER=test php ./bin/phpunit -c phpunit.xml.dist ./tests/

code-sniffer-cli:
	$(COMPOSE) run --rm --no-deps -e USER=test php ./bin/phpcs --extensions=php --standard=psr2 ./src

php-doc: install-phpdoc
	$(COMPOSE) run --rm --no-deps -e USER=test php ./bin/phpdoc -d ./src -t ./docs/api
