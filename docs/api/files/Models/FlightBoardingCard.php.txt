<?php
/**
 * FlightBoardingCard
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\Models;

use InvalidArgumentException;
use TripSorter\ValueObjects\Point;
use TripSorter\ValueObjects\TransportType;

/**
 * Model for flight boarding card
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
class FlightBoardingCard extends BoardingCard
{
    /**
     * Flight identifier
     *
     * @var string
     */
    protected $identifier;

    /**
     * Gate identifier
     *
     * @var string
     */
    protected $gate;

    /**
     * Baggage ticket counter
     *
     * @var string
     */
    protected $baggageTicketCounter;

    /**
     * Required attributes
     *
     * @var array
     */
    protected static $requiredAttributes = [
        'departure',
        'arrival',
        'seat',
        'identifier',
        'gate',
        'baggage_ticket_counter'
    ];

    /**
     * FlightBoardingCard constructor.
     *
     * @param Point  $departurePoint       - departure point
     * @param Point  $arrivalPoint         - arrival point
     * @param string $seat                 - seat assignment
     * @param string $identifier           - flight identifier
     * @param string $gate                 - gate identifier
     * @param string $baggageTicketCounter - baggage ticket counter
     */
    public function __construct(
        Point  $departurePoint,
        Point  $arrivalPoint,
        string $seat,
        string $identifier,
        string $gate,
        string $baggageTicketCounter = null
    ) {
        parent::__construct($departurePoint, $arrivalPoint, $seat);
        $this->identifier = $identifier;
        $this->gate = $gate;
        $this->baggageTicketCounter = $baggageTicketCounter
            ?: 'Automatically transferred';
    }

    /**
     * Helper method to create card from array
     *
     * @param array $cardItem - card detail in array format
     *
     * @return FlightBoardingCard
     */
    public static function fromArray(array $cardItem)
    {
        $missingKeys = array_diff(
            static::$requiredAttributes,
            array_keys($cardItem)
        );
        if (count($missingKeys) > 0) {
            throw new InvalidArgumentException(
                sprintf(
                    "Flight boarding card missing keys: [%s]",
                    json_encode($missingKeys)
                )
            );
        }
        return new self(
            new Point($cardItem['departure']),
            new Point($cardItem['arrival']),
            $cardItem['seat'],
            $cardItem['identifier'],
            $cardItem['gate'],
            $cardItem['baggage_ticket_counter']
        );
    }

    /**
     * Magic method to support treating the object as string
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Take the flight %s from %s to %s. Gate : %s. Seat : %s. ' .
            'Baggage ticket counter : %s.',
            $this->identifier,
            (string) $this->departurePoint,
            (string) $this->arrivalPoint,
            $this->gate,
            $this->seat,
            $this->baggageTicketCounter
        );
    }

    /**
     * Serializes the card to an array that can be serialized
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'transport_type' => TransportType::FLIGHT,
            'identifier' => $this->identifier,
            'departure' => (string) $this->departurePoint,
            'arrival' => (string) $this->arrivalPoint,
            'gate' => $this->gate,
            'seat' => $this->seat,
            'baggage_ticket_counter' => $this->baggageTicketCounter
        ];
    }
}

