<?php
require_once __DIR__ . '/../vendor/autoload.php';

use TripSorter\Models\BoardingCardCollection;
use TripSorter\Services\BoardingCardFactory;
use TripSorter\Services\TripService;

// Restrict access from localhost
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !(in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', 'fe80::1', '::1'], true) || PHP_SAPI === 'cli-server')
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

$post = file_get_contents('php://input');
$cardItems = json_decode($post, true);

$factory = new BoardingCardFactory();
$collection = new BoardingCardCollection();

$service = new TripService($factory, $collection);

try {
    // Returning the sorted boarding cards in JSON format
    $response = [
        'success' => true,
        'cards' => $service->sortBoardingCards($cardItems)
    ];

//    // Returning the trip itinerary in human readable format
//    $response = [
//        'success' => true,
//        'itinerary' => $service->printItinerary($cardItems)
//    ];
} catch (Exception $exception) {
    $response = [
        'success' => false,
        'message' => $exception->getMessage()
    ];
}

echo json_encode($response);
