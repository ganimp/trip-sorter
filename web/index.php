<?php

$cards = [
    [
        'transport_type' => 'flight',
        'departure' => 'Stockholm',
        'arrival' => 'New York JFK',
        'identifier' => 'SK22',
        'gate' => '22',
        'seat' => '7B',
        'baggage_ticket_counter' => null,
    ],
    [
        'transport_type' => 'airport_bus',
        'departure' => 'Barcelona',
        'arrival' => 'Gerona Airport',
        'seat' => null
    ],
    [
        'transport_type' => 'flight',
        'departure' => 'Gerona Airport',
        'arrival' => 'Stockholm',
        'identifier' => 'SK455',
        'gate' => '45B',
        'seat' => '3A',
        'baggage_ticket_counter' => '344',
    ],
    [
        'transport_type' => 'train',
        'departure' => 'Madrid',
        'arrival' => 'Barcelona',
        'identifier' => '78A',
        'seat' => '45B'
    ]
];

printSeparatorLine();
printText('Unordered boarding cards:');
printSeparatorLine();
printArray($cards);

$cardsJson = json_encode($cards);

// create curl resource
$ch = curl_init();

//curl_setopt($ch, CURLOPT_VERBOSE, 1);

curl_setopt($ch, CURLOPT_URL, "http://localhost/api.php");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $cardsJson);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($cardsJson))
);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec($ch);

curl_close($ch);

$response = json_decode($response, true);

printSeparatorLine();
if ($response['success']) {
    printText('Successfuly sorted boarding cards');
    printText('Sorted boarding cards:');
    printArray($response['cards']);
} else {
    printText('Failed to sorted boarding cards');
    printText('Error: ' . $response['message']);
}

function printSeparatorLine()
{
    printText(str_repeat('-', 80));
}

function printText(string $content)
{
    echo $content . PHP_EOL;
}

function printArray(array $content)
{
    print_r($content) . PHP_EOL;
}