<?php
namespace TripSorter\Tests\Models;

use PHPUnit\Framework\TestCase;
use TripSorter\Models\AirportBusBoardingCard;
use TripSorter\ValueObjects\TransportType;

class AirportBusBoardingCardTest extends TestCase
{
    public function testWeCanCreateFromArray()
    {
        $cardItem = [
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => null
        ];
        $card = AirportBusBoardingCard::fromArray($cardItem);
        $this->assertInstanceOf(AirportBusBoardingCard::class, $card);
    }

    /**
     * @dataProvider provideInvalidAirportBusCardItems
     */
    public function testItThrowsExceptionWhenAttributesMissingInArray(
        array $cardItem
    ) {
        $this->expectException(\InvalidArgumentException::class);
        AirportBusBoardingCard::fromArray($cardItem);
    }

    public function provideInvalidAirportBusCardItems()
    {
        $cardItemMissingDeparture = [
            'arrival' => 'Gerona Airport',
            'seat' => null
        ];
        $cardItemMissingArrival = [
            'departure' => 'Barcelona',
            'seat' => null
        ];
        $cardItemMissingSeat = [
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
        ];
        return [
            'missing departure' => [$cardItemMissingDeparture],
            'missing arrival' => [$cardItemMissingArrival],
            'missing seat' => [$cardItemMissingSeat],
        ];
    }

    public function testWeCanTreatItAsString()
    {
        $cardItem = [
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => null
        ];
        $card = AirportBusBoardingCard::fromArray($cardItem);
        $expectedString = 'Take the airport bus from Barcelona to Gerona Airport. Seat : No seat assignment';
        $this->assertEquals($expectedString, (string) $card);
    }

    public function testItCanBeJsonEncoded()
    {
        $cardItem = [
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => null
        ];
        $card = AirportBusBoardingCard::fromArray($cardItem);
        $expectedJson = json_encode([
            'transport_type' => TransportType::AIRPORT_BUS,
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => 'No seat assignment'
        ]);
        $this->assertEquals($expectedJson, json_encode($card));
    }
}
