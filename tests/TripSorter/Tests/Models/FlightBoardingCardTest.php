<?php
namespace TripSorter\Tests\Models;

use PHPUnit\Framework\TestCase;
use TripSorter\Models\FlightBoardingCard;
use TripSorter\ValueObjects\TransportType;

class FlightBoardingCardTest extends TestCase
{
    public function testWeCanCreateFromArray()
    {
        $cardItem = [
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'seat' => '3A',
            'identifier' => 'SK455',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'

        ];
        $card = FlightBoardingCard::fromArray($cardItem);
        $this->assertInstanceOf(FlightBoardingCard::class, $card);
    }

    /**
     * @dataProvider provideInvalidFlightCardItems
     */
    public function testItThrowsExceptionWhenAttributesMissingInArray(
        array $cardItem
    ) {
        $this->expectException(\InvalidArgumentException::class);
        FlightBoardingCard::fromArray($cardItem);
    }

    public function provideInvalidFlightCardItems()
    {
        $cardItemMissingDeparture = [
            'arrival' => 'Stockholm',
            'seat' => '3A',
            'identifier' => 'SK455',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'
        ];
        $cardItemMissingArrival = [
            'departure' => 'Gerona Airport',
            'seat' => '3A',
            'identifier' => 'SK455',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'
        ];
        $cardItemMissingSeat = [
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'identifier' => 'SK455',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'
        ];
        $cardItemMissingIdentifier = [
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'seat' => '3A',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'
        ];
        $cardItemMissingGate = [
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'seat' => '3A',
            'identifier' => 'SK455',
            'baggage_ticket_counter' => '344'
        ];
        $cardItemMissingBaggageTicketCounter = [
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'seat' => '3A',
            'identifier' => 'SK455',
            'gate' => '45B',
        ];
        return [
            'missing departure' => [$cardItemMissingDeparture],
            'missing arrival' => [$cardItemMissingArrival],
            'missing seat' => [$cardItemMissingSeat],
            'missing identifier' => [$cardItemMissingIdentifier],
            'missing gate' => [$cardItemMissingGate],
            'missing baggage ticket counter' => [$cardItemMissingBaggageTicketCounter],
        ];
    }

    public function testWeCanTreatItAsString()
    {
        $cardItem = [
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'seat' => '3A',
            'identifier' => 'SK455',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'
        ];
        $card = FlightBoardingCard::fromArray($cardItem);
        $expectedString = 'Take the flight SK455 from Gerona Airport to Stockholm. Gate : 45B. Seat : 3A. Baggage ticket counter : 344.';
        $this->assertEquals($expectedString, (string) $card);
    }

    public function testItCanBeJsonEncoded()
    {
        $cardItem = [
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'identifier' => 'SK455',
            'seat' => '3A',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'
        ];
        $card = FlightBoardingCard::fromArray($cardItem);
        $expectedJson = json_encode([
            'transport_type' => TransportType::FLIGHT,
            'identifier' => 'SK455',
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'gate' => '45B',
            'seat' => '3A',
            'baggage_ticket_counter' => '344'
        ]);
        $this->assertEquals($expectedJson, json_encode($card));
    }
}
