<?php
namespace TripSorter\Tests\Models;

use PHPUnit\Framework\TestCase;
use TripSorter\Models\TrainBoardingCard;
use TripSorter\ValueObjects\TransportType;

class TrainBoardingCardTest extends TestCase
{
    public function testWeCanCreateFromArray()
    {
        $cardItem = [
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'seat' => '45B',
            'identifier' => '78A'
        ];
        $card = TrainBoardingCard::fromArray($cardItem);
        $this->assertInstanceOf(TrainBoardingCard::class, $card);
    }

    /**
     * @dataProvider provideInvalidTrainCardItems
     */
    public function testItThrowsExceptionWhenAttributesMissingInArray(
        array $cardItem
    ) {
        $this->expectException(\InvalidArgumentException::class);
        TrainBoardingCard::fromArray($cardItem);
    }

    public function provideInvalidTrainCardItems()
    {
        $cardItemMissingDeparture = [
            'arrival' => 'Barcelona',
            'seat' => '45B',
            'identifier' => '78A'
        ];
        $cardItemMissingArrival = [
            'departure' => 'Madrid',
            'seat' => '45B',
            'identifier' => '78A'
        ];
        $cardItemMissingSeat = [
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'identifier' => '78A'
        ];
        $cardItemMissingIdentifier = [
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'seat' => '45B',
        ];
        return [
            'missing departure' => [$cardItemMissingDeparture],
            'missing arrival' => [$cardItemMissingArrival],
            'missing seat' => [$cardItemMissingSeat],
            'missing identifier' => [$cardItemMissingIdentifier],
        ];
    }

    public function testWeCanTreatItAsString()
    {
        $cardItem = [
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'seat' => '45B',
            'identifier' => '78A'
        ];
        $card = TrainBoardingCard::fromArray($cardItem);
        $expectedString = 'Take train 78A from Madrid to Barcelona. Seat : 45B.';
        $this->assertEquals($expectedString, (string) $card);
    }

    public function testItCanBeJsonEncoded()
    {
        $cardItem = [
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'seat' => '45B',
            'identifier' => '78A'
        ];
        $card = TrainBoardingCard::fromArray($cardItem);
        $expectedJson = json_encode([
            'transport_type' => TransportType::TRAIN,
            'identifier' => '78A',
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'seat' => '45B'
        ]);
        $this->assertEquals($expectedJson, json_encode($card));
    }
}
