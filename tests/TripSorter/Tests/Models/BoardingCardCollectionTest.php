<?php
namespace FeedReader\Tests\Models;

use JsonSerializable;
use PHPUnit\Framework\TestCase;
use TripSorter\Exceptions\FailedSortingBoardingCardsException;
use TripSorter\Models\AirportBusBoardingCard;
use TripSorter\Models\BoardingCardCollection;
use TripSorter\Models\TrainBoardingCard;
use TripSorter\ValueObjects\TransportType;

class BoardingCardCollectionTest extends TestCase
{
    /**
     * @dataProvider provideBoardingCards
     * @param array $cards
     */
    public function testWeCanAddCardsAndResetCollection(
        array $cards
    ) {
        $collection = new BoardingCardCollection();
        $this->assertCount(0, $collection);
        foreach ($cards as $card) {
            $collection->add($card);
        }
        $this->assertCount(count($cards), $collection);
        $collection->reset();
        $this->assertCount(0, $collection);
    }

    public function provideBoardingCards()
    {
        list(,$busCard) = $this->getBusCard();
        list(,$trainCard) = $this->getTrainCard();
        $cards = [$busCard, $trainCard];
        return [
            'airport bus and train boarding cards' => [$cards],
        ];
    }

    /**
     * @dataProvider provideBoardingCardsWithExpectedOutput
     * @param string $expectedString
     * @param array $cards
     */
    public function testWeCanJsonSerialize(
        string $expectedString,
        array $cards
    ) {
        $collection = new BoardingCardCollection();
        $this->assertCount(0, $collection);
        foreach ($cards as $card) {
            $collection->add($card);
        }
        $this->assertInstanceOf(JsonSerializable::class, $collection);
        $this->assertEquals($expectedString, json_encode($collection));
    }

    public function provideBoardingCardsWithExpectedOutput()
    {
        list($busCardItem, $busCard) = $this->getBusCard();
        list($trainCardItem, $trainCard) = $this->getTrainCard();
        $cards = [$busCard, $trainCard];

        $expectedString = json_encode([$busCardItem, $trainCardItem]);

        return [
            'cards to json' => [$expectedString, $cards],
        ];
    }

    /**
     * @dataProvider provideBoardingCards
     */
    public function testWeCanTreatItAsString(
        array $cards
    ) {
        $collection = new BoardingCardCollection();
        $this->assertCount(0, $collection);
        foreach ($cards as $card) {
            $collection->add($card);
        }
        $expectedString = 'Take the airport bus from Barcelona to Gerona Airport. Seat : 12B
Take train 78A from Madrid to Barcelona. Seat : 45B.
You have arrived at your final destination.';
        $this->assertEquals($expectedString, (string) $collection);
    }

    /**
     * @dataProvider provideUnorderedAndSortedCards
     */
    public function testWeCanSortUnorderedCards(
        array $expected,
        array $cards
    ) {
        $collection = new BoardingCardCollection();
        $this->assertCount(0, $collection);
        foreach ($cards as $card) {
            $collection->add($card);
        }
        $this->assertEquals(
            json_encode($expected),
            json_encode($collection->sort())
        );
    }

    public function provideUnorderedAndSortedCards()
    {
        list($busCardItem, $busCard) = $this->getBusCard();
        list($trainCardItem, $trainCard) = $this->getTrainCard();
        $cards = [$busCard, $trainCard];
        $expected = [$trainCardItem, $busCardItem];

        return [
            'unsorted and sorted cards' => [$expected, $cards],
        ];
    }

    public function testItThrowsExceptionWhenAttemptingToSortEmptyCollection() {
        $collection = new BoardingCardCollection();
        $this->expectException(\Exception::class);
        $collection->sort();
    }

    /**
     * @dataProvider provideUnrelatedCards
     */
    public function testItThrowsExceptionWhenAttemptingToSortInvalidCollection(
        array $cards
    ) {
        $collection = new BoardingCardCollection();
        foreach ($cards as $card) {
            $collection->add($card);
        }
        $this->expectException(FailedSortingBoardingCardsException::class);
        $collection->sort();
    }

    public function provideUnrelatedCards()
    {
        list(, $busCard) = $this->getBusCard(
            $arrival = 'Madrid'
        );
        list(, $trainCard) = $this->getTrainCard();
        $cards = [$busCard, $trainCard];

        return [
            'unrelated cards' => [$cards],
        ];
    }

    /**
     * @param string $arrival
     * @return array
     */
    private function getBusCard($arrival = 'Gerona Airport'): array
    {
        $busCardItem = [
            'transport_type' => TransportType::AIRPORT_BUS,
            'departure' => 'Barcelona',
            'arrival' => $arrival,
            'seat' => '12B'
        ];
        $busCard = AirportBusBoardingCard::fromArray($busCardItem);
        return [$busCardItem, $busCard];
    }

    /**
     * @return array
     */
    private function getTrainCard(): array
    {
        $trainCardItem = [
            'transport_type' => TransportType::TRAIN,
            'identifier' => '78A',
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'seat' => '45B'
        ];
        $trainCard = TrainBoardingCard::fromArray($trainCardItem);
        return [$trainCardItem, $trainCard];
    }
}
