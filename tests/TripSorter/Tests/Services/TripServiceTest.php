<?php
namespace TripSorter\Tests\ValueObjects;

use Mockery;
use PHPUnit\Framework\TestCase;
use TripSorter\Models\AirportBusBoardingCard;
use TripSorter\Models\BoardingCard;
use TripSorter\Models\BoardingCardCollection;
use TripSorter\Services\BoardingCardFactory;
use TripSorter\Services\TripService;
use TripSorter\ValueObjects\TransportType;

class TripServiceTest extends TestCase
{
    /**
     * @dataProvider provideCardItemsAndExpectedInstanceFqn
     */
    public function testSortBoardingCards(
        array $cardItem,
        BoardingCard $card
    ) {
        $factory = Mockery::mock(BoardingCardFactory::class);
        $factory->shouldReceive('build')
            ->once()
            ->with($cardItem)
            ->andReturn($card);
        $collection = Mockery::mock(BoardingCardCollection::class);
        $collection->shouldReceive('add')
            ->once()
            ->andReturn($collection);
        $collection->shouldReceive('sort')
            ->once()
            ->andReturn($collection);
        $service = new TripService(
            $factory,
            $collection
        );
        $this->assertEquals(
            $collection,
            $service->sortBoardingCards([$cardItem])
        );
    }

    /**
     * @dataProvider provideCardItemsAndExpectedInstanceFqn
     */
    public function testPrintTripItinerary(
        array $cardItem,
        BoardingCard $card
    ) {
        $factory = Mockery::mock(BoardingCardFactory::class);
        $factory->shouldReceive('build')
            ->once()
            ->with($cardItem)
            ->andReturn($card);
        $collection = Mockery::mock(BoardingCardCollection::class);
        $collection->shouldReceive('add')
            ->once()
            ->andReturn($collection);
        $collection->shouldReceive('sort')
            ->once()
            ->andReturn($collection);
        $collection->shouldReceive('__toString')
            ->once()
            ->andReturn((string) $card);
        $service = new TripService(
            $factory,
            $collection
        );
        $this->assertEquals(
            'Take the airport bus from Barcelona to Gerona Airport. Seat : 12B',
            $service->printItinerary([$cardItem])
        );
    }

    public function provideCardItemsAndExpectedInstanceFqn()
    {
        list($busCardItem, $busCard) = $this->getBusCard();

        return [
            'bus boarding card' => [$busCardItem, $busCard],
        ];
    }

    /**
     * @return array
     */
    private function getBusCard(): array
    {
        $busCardItem = [
            'transport_type' => TransportType::AIRPORT_BUS,
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => '12B'
        ];
        $busCard = AirportBusBoardingCard::fromArray($busCardItem);
        return [$busCardItem, $busCard];
    }
}
