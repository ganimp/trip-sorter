<?php
namespace TripSorter\Tests\ValueObjects;

use PHPUnit\Framework\TestCase;
use TripSorter\Models\AirportBusBoardingCard;
use TripSorter\Models\FlightBoardingCard;
use TripSorter\Models\TrainBoardingCard;
use TripSorter\Services\BoardingCardFactory;
use TripSorter\ValueObjects\TransportType;

class BoardingCardFactoryTest extends TestCase
{
    /**
     * @dataProvider provideCardItemsAndExpectedInstanceFqn
     */
    public function testBuild(
        string $expectedInstanceFqn,
        array $cardItem
    ) {
        $this->assertInstanceOf(
            $expectedInstanceFqn,
            BoardingCardFactory::build($cardItem)
        );
    }

    public function provideCardItemsAndExpectedInstanceFqn()
    {
        list($busCardItem,) = $this->getBusCard();
        list($trainCardItem,) = $this->getTrainCard();
        list($flightCardItem,) = $this->getFlightCard();
        return [
            'bus boarding card' => [AirportBusBoardingCard::class, $busCardItem],
            'train boarding card' => [TrainBoardingCard::class, $trainCardItem],
            'flight boarding card' => [FlightBoardingCard::class, $flightCardItem],
        ];
    }

    public function testBuildThrowsExceptionWhenTransportTypeIsMissing()
    {
        $this->expectException(\InvalidArgumentException::class);
        $busCardItem = [
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => '12B'
        ];
        BoardingCardFactory::build($busCardItem);
    }

    public function testBuildThrowsExceptionWhenTransportTypeIsInvalid()
    {
        $this->expectException(\InvalidArgumentException::class);
        $busCardItem = [
            'transport_type' => 'invalid',
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => '12B'
        ];
        BoardingCardFactory::build($busCardItem);
    }

    /**
     * @return array
     */
    private function getBusCard(): array
    {
        $busCardItem = [
            'transport_type' => TransportType::AIRPORT_BUS,
            'departure' => 'Barcelona',
            'arrival' => 'Gerona Airport',
            'seat' => '12B'
        ];
        $busCard = AirportBusBoardingCard::fromArray($busCardItem);
        return [$busCardItem, $busCard];
    }

    /**
     * @return array
     */
    private function getTrainCard(): array
    {
        $trainCardItem = [
            'transport_type' => TransportType::TRAIN,
            'identifier' => '78A',
            'departure' => 'Madrid',
            'arrival' => 'Barcelona',
            'seat' => '45B'
        ];
        $trainCard = TrainBoardingCard::fromArray($trainCardItem);
        return [$trainCardItem, $trainCard];
    }

    private function getFlightCard()
    {
        $flightCardItem = [
            'transport_type' => TransportType::FLIGHT,
            'departure' => 'Gerona Airport',
            'arrival' => 'Stockholm',
            'seat' => '3A',
            'identifier' => 'SK455',
            'gate' => '45B',
            'baggage_ticket_counter' => '344'

        ];
        $flightCard = FlightBoardingCard::fromArray($flightCardItem);
        return [$flightCardItem, $flightCard];
    }
}
