<?php
namespace TripSorter\Tests\ValueObjects;

use PHPUnit\Framework\TestCase;
use TripSorter\ValueObjects\Point;

class PointTest extends TestCase
{
    public function testConstructor()
    {
        $point = new Point('Barcelona');
        $this->assertInstanceOf(Point::class, $point);
    }

    public function testGetter()
    {
        $pointName = 'Barcelona';
        $point = new Point($pointName);
        $this->assertEquals($pointName, $point->getName());
    }

    public function testWeCanTreatItAsString()
    {
        $pointName = 'Barcelona';
        $point = new Point($pointName);
        $this->assertEquals($pointName, (string) $point);
    }
}
