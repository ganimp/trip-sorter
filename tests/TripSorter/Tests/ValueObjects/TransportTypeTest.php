<?php
namespace TripSorter\Tests\ValueObjects;

use PHPUnit\Framework\TestCase;
use TripSorter\ValueObjects\TransportType;

class TransportTypeTest extends TestCase
{
    public function testConstructor()
    {
        $transportType = new TransportType(TransportType::AIRPORT_BUS);
        $this->assertInstanceOf(TransportType::class, $transportType);
    }

    public function testConstructorFailsOnInvalidTransportType()
    {
        $this->expectException(\InvalidArgumentException::class);
        new TransportType('invalid transport type');
    }

    public function testWeCanTreatItAsString()
    {
        $transportType = new TransportType(TransportType::TRAIN);
        $this->assertEquals(TransportType::TRAIN, (string) $transportType);
    }
}
