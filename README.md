# Backend Developer Assignment

Algorithm to sort a stack of boarding cards for various transportations.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

You will need

* PHP 5.3.2 or above
* Docker
* Docker Compose
* GNU make utility

## Setup

This repo includes a set of **GNU Make** utility commands that will help you to build and manage the application quick and fast.

```
make start-app
```

### Checking docker container status

```
make state
```

### Stopping docker container

```
make down
```

## Manual testing

```
make enter-container
cd web;
nohup php -S localhost:80 > phpd.log 2>&1 &
php index.php
```
### Sample 
```php
    --------------------------------------------------------------------------------
    Unordered boarding cards:
    --------------------------------------------------------------------------------
    Array
    (
        [0] => Array
            (
                [transport_type] => flight
                [departure] => Stockholm
                [arrival] => New York JFK
                [identifier] => SK22
                [gate] => 22
                [seat] => 7B
                [baggage_ticket_counter] =>
            )
    
        [1] => Array
            (
                [transport_type] => airport_bus
                [departure] => Barcelona
                [arrival] => Gerona Airport
                [seat] =>
            )
    
        [2] => Array
            (
                [transport_type] => flight
                [departure] => Gerona Airport
                [arrival] => Stockholm
                [identifier] => SK455
                [gate] => 45B
                [seat] => 3A
                [baggage_ticket_counter] => 344
            )
    
        [3] => Array
            (
                [transport_type] => train
                [departure] => Madrid
                [arrival] => Barcelona
                [identifier] => 78A
                [seat] => 45B
            )
    
    )
    --------------------------------------------------------------------------------
    Successfuly sorted boarding cards
    Sorted boarding cards:
    Array
    (
        [0] => Array
            (
                [transport_type] => train
                [identifier] => 78A
                [departure] => Madrid
                [arrival] => Barcelona
                [seat] => 45B
            )
    
        [1] => Array
            (
                [transport_type] => airport_bus
                [departure] => Barcelona
                [arrival] => Gerona Airport
                [seat] => No seat assignment
            )
    
        [2] => Array
            (
                [transport_type] => flight
                [identifier] => SK455
                [departure] => Gerona Airport
                [arrival] => Stockholm
                [gate] => 45B
                [seat] => 3A
                [baggage_ticket_counter] => 344
            )
    
        [3] => Array
            (
                [transport_type] => flight
                [identifier] => SK22
                [departure] => Stockholm
                [arrival] => New York JFK
                [gate] => 22
                [seat] => 7B
                [baggage_ticket_counter] => Automatically transferred
            )
    
    )
```

## Automated testing

Frameworks used,

* [PHPUnit](https://phpunit.de/) - Unit testing framework for PHP.
* [Mockery](http://docs.mockery.io/en/latest/)   - PHP mock object framework for use in unit testing with PHPUnit.
* [PHP_CodeSniffer](https://www.squizlabs.com/php-codesniffer) - Detects violations of a defined set of coding standards.
* [phpDocumentor](https://phpdoc.org/) - analyzes PHP code and create documentation.

## Running the tests

### Unit test

```
make test
```

### And coding style tests

```
make code-sniffer-cli
```

## Generate php-doc

```
make php-doc
```

## Authors

* **Gani** 

## License

This project is licensed under the MIT License
