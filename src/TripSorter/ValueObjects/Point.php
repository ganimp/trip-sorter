<?php
/**
 * Point
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\ValueObjects;

/**
 * Point value object for departure/ arrival stops in boarding card
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
final class Point extends ValueObject
{
    /**
     * Departure/ arrival stop name
     *
     * @var string
     */
    protected $name;

    /**
     * Point constructor.
     *
     * @param string $name - name of the point
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Returns the name of the point
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Magic method to support treating the object as string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
