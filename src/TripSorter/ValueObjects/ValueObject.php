<?php
/**
 * ValueObject
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\ValueObjects;

/**
 * Abstracted value object
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
abstract class ValueObject implements IValueObject
{
    /**
     * Compare value objects
     *
     * @param IValueObject $other - Value object to compare
     *
     * @return bool
     */
    public function equals(IValueObject $other): bool
    {
        if (get_class($this) !== get_class($other)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'A Value Object of type %s can not be compared' .
                    'to another of type %s',
                    get_class($this),
                    get_class($other)
                )
            );
        }

        return $this == $other;
    }
}
