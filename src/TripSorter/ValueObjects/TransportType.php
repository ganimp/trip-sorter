<?php
/**
 * TransportType
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\ValueObjects;

/**
 * Point
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
final class TransportType extends ValueObject
{
    const TRAIN = 'train';

    const AIRPORT_BUS = 'airport_bus';

    const FLIGHT = 'flight';

    /**
     * Transport type
     *
     * @var string
     */
    protected $value;


    protected $allowedTypes = [
        self::TRAIN,
        self::AIRPORT_BUS,
        self::FLIGHT
    ];

    /**
     * Point constructor.
     *
     * @param string $value -  transport type
     */
    public function __construct(string $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    /**
     * Validates the transport type.
     *
     * @param string $value - name of the transport type
     *
     * @return void
     */
    protected function validate(string $value)
    {
        if (!in_array($value, $this->allowedTypes)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid transport type [%s].",
                    $value
                )
            );
        }
    }

    /**
     * Magic method to support treating the object as string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
