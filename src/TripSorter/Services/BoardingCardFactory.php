<?php
/**
 * BoardingCardFactory
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\Services;

use TripSorter\Models\AirportBusBoardingCard;
use TripSorter\Models\BoardingCard;
use TripSorter\Models\FlightBoardingCard;
use TripSorter\Models\TrainBoardingCard;
use TripSorter\ValueObjects\TransportType;
use InvalidArgumentException;

/**
 * Factory to build boarding cards by transport type
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
class BoardingCardFactory
{
    /**
     * Static method to build boarding cards by transport type
     *
     * @param array $cardItem - Card details in array format
     *
     * @return BoardingCard
     */
    public static function build(array $cardItem): BoardingCard
    {
        if (!array_key_exists('transport_type', $cardItem)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid boarding card details specified. Missing transport_type'
                )
            );
        }
        $type = new TransportType($cardItem['transport_type']);
        switch ($type) {
            case TransportType::TRAIN:
                return TrainBoardingCard::fromArray($cardItem);
            case TransportType::AIRPORT_BUS:
                return AirportBusBoardingCard::fromArray($cardItem);
            case TransportType::FLIGHT:
                return FlightBoardingCard::fromArray($cardItem);
        }
    }
}
