<?php
/**
 * TripService
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\Services;

use TripSorter\Models\BoardingCardCollection;

/**
 * Service to sort boarding cards
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
class TripService
{
    /**
     * Factory for building boarding cards
     *
     * @var BoardingCardFactory
     */
    protected $factory;

    /**
     * Collection of boarding cards
     *
     * @var BoardingCardCollection
     */
    protected $collection;

    /**
     * TripService constructor.
     *
     * @param BoardingCardFactory    $factory    - factory to build boarding cards
     * @param BoardingCardCollection $collection - collection of boarding cards
     */
    public function __construct(
        BoardingCardFactory $factory,
        BoardingCardCollection $collection
    ) {
        $this->factory = $factory;
        $this->collection = $collection;
    }

    /**
     * Method to sort boarding cards
     *
     * @param array $cardItems - Card details in array format
     *
     * @return BoardingCardCollection
     */
    public function sortBoardingCards(array $cardItems)
    {
        foreach ($cardItems as $cardItem) {
            $this->collection->add(
                $this->factory->build($cardItem)
            );
        }
        return $this->collection->sort();
    }

    /**
     * Prints the trip itinerary in human readable format
     *
     * @param array $cardItems - Card details in array format
     *
     * @return string
     */
    public function printItinerary(array $cardItems)
    {
        return (string) $this->sortBoardingCards($cardItems);
    }
}
