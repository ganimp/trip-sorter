<?php
/**
 * BoardingCard
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\Models;

use TripSorter\ValueObjects\Point;
use JsonSerializable;

/**
 * Abstracted boarding card
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
abstract class BoardingCard implements JsonSerializable
{
    /**
     * Departure point
     *
     * @var Point
     */
    protected $departurePoint;

    /**
     * Arrival point
     *
     * @var Point
     */
    protected $arrivalPoint;

    /**
     * Seat assignment
     *
     * @var string
     */
    protected $seat;

    /**
     * BoardingCard constructor.
     *
     * @param Point       $departurePoint - departure point
     * @param Point       $arrivalPoint   - arrival point
     * @param string|null $seat           - seat assignment
     */
    public function __construct(
        Point $departurePoint,
        Point $arrivalPoint,
        string $seat = null
    ) {
        $this->departurePoint = $departurePoint;
        $this->arrivalPoint = $arrivalPoint;
        $this->seat = $seat ?: 'No seat assignment';
    }

    /**
     * Returns the arrival point
     *
     * @return Point
     */
    public function getArrivalPoint(): Point
    {
        return $this->arrivalPoint;
    }

    /**
     * Returns the departure point
     *
     * @return Point
     */
    public function getDeparturePoint(): Point
    {
        return $this->departurePoint;
    }

    /**
     * Magic method to support treating the object as string
     *
     * @return mixed
     */
    abstract public function __toString();

    /**
     * Serializes the card to an array that can be serialized
     *
     * @return mixed
     */
    abstract public function jsonSerialize();
}
