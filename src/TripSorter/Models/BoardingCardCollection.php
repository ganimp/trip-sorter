<?php
/**
 * BoardingCardCollection
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\Models;

use Countable;
use JsonSerializable;
use TripSorter\Exceptions\FailedSortingBoardingCardsException;

/**
 * Collection of boarding cards
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
class BoardingCardCollection implements Countable, JsonSerializable
{
    /**
     * Collection of boarding cards
     *
     * @var BoardingCard[]
     */
    protected $cards = [];

    /**
     * Adds a card to the collection of boarding cards
     *
     * @param BoardingCard $card - card model
     *
     * @return BoardingCardCollection
     */
    public function add(BoardingCard $card): BoardingCardCollection
    {
        $this->cards[] = $card;
        return $this;
    }

    /**
     * Resets the collection
     *
     * @return BoardingCardCollection
     */
    public function reset(): BoardingCardCollection
    {
        $this->cards = [];
        return $this;
    }

    /**
     * Allows to use the object in count() function
     *
     * @return int
     */
    public function count() : int
    {
        return count($this->cards);
    }

    /**
     * Serializes the card collection to an array that can be serialized
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->cards;
    }

    /**
     * Magic method to support treating the object as string
     *
     * @return string
     */
    public function __toString()
    {
        $output = '';
        foreach ($this->cards as $card) {
            $output .= (string) $card . PHP_EOL;
        }
        $output .= 'You have arrived at your final destination.';
        return $output;
    }

    /**
     * Sorting boarding cards in the collection
     *
     * @return BoardingCardCollection
     * @throws FailedSortingBoardingCardsException
     * @throws \Exception
     */
    public function sort() :  BoardingCardCollection
    {
        if (empty($this->cards)) {
            throw new \Exception('No boarding cards to sort.');
        }

        $departureMap = [];
        $arrivalMap = [];
        $start = null;
        foreach ($this->cards as $card) {
            $departureMap[$card->getDeparturePoint()->getName()] = $card;
            $arrivalMap[$card->getArrivalPoint()->getName()] = $card;
        }

        foreach ($this->cards as $card) {
            $departure = $card->getDeparturePoint()->getName();
            if (!array_key_exists($departure, $arrivalMap)) {
                $start = $card;
                break;
            }
        }
        if ($start == null) {
            throw new FailedSortingBoardingCardsException(
                sprintf(
                    'Failed to find starting point for boarding cards: [%s]',
                    json_encode($this)
                )
            );
        }
        $this->reset();
        $nextPoint = $start->getDeparturePoint()->getName();
        while (array_key_exists($nextPoint, $departureMap)) {
            $current = $departureMap[$nextPoint];
            $this->add($current);
            $nextPoint = $current->getArrivalPoint()->getName();
        }
        return $this;
    }
}
