<?php
/**
 * AirportBusBoardingCard
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\Models;

use InvalidArgumentException;
use TripSorter\ValueObjects\Point;
use TripSorter\ValueObjects\TransportType;

/**
 * Model for airport-bus boarding card
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
class AirportBusBoardingCard extends BoardingCard
{
    /**
     * Required attributes
     *
     * @var array
     */
    protected static $requiredAttributes = ['departure', 'arrival', 'seat'];

    /**
     * Helper method to create card from array
     *
     * @param array $cardItem - card details in array format
     *
     * @return AirportBusBoardingCard
     */
    public static function fromArray(array $cardItem)
    {
        $missingKeys = array_diff(
            static::$requiredAttributes,
            array_keys($cardItem)
        );
        if (count($missingKeys) > 0) {
            throw new InvalidArgumentException(
                sprintf(
                    "Airport bus boarding card missing keys: [%s]",
                    json_encode($missingKeys)
                )
            );
        }
        return new self(
            new Point($cardItem['departure']),
            new Point($cardItem['arrival']),
            $cardItem['seat']
        );
    }

    /**
     * Magic method to support treating the object as string
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Take the airport bus from %s to %s. Seat : %s',
            (string) $this->departurePoint,
            (string) $this->arrivalPoint,
            $this->seat
        );
    }

    /**
     * Serializes the card to an array that can be serialized
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'transport_type' => TransportType::AIRPORT_BUS,
            'departure' => (string) $this->departurePoint,
            'arrival' => (string) $this->arrivalPoint,
            'seat' => $this->seat
        ];
    }
}
