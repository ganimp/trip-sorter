<?php
/**
 * TrainBoardingCard
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
namespace TripSorter\Models;

use InvalidArgumentException;
use TripSorter\ValueObjects\Point;
use TripSorter\ValueObjects\TransportType;

/**
 * Model for train boarding card
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  TripSorter
 * @author   Gani <ganicvns@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://bitbucket.org/ganimp/
 */
class TrainBoardingCard extends BoardingCard
{
    /**
     * Train identifier
     *
     * @var string
     */
    protected $identifier;

    /**
     * Required attributes
     *
     * @var array
     */
    protected static $requiredAttributes = [
        'departure',
        'arrival',
        'seat',
        'identifier'
    ];

    /**
     * TrainBoardingCard constructor.
     *
     * @param Point  $departurePoint - departure point
     * @param Point  $arrivalPoint   - arrival point
     * @param string $seat           - seat assignment
     * @param string $identifier     - train identifier
     */
    public function __construct(
        Point $departurePoint,
        Point $arrivalPoint,
        string $seat,
        string $identifier
    ) {
        parent::__construct($departurePoint, $arrivalPoint, $seat);
        $this->identifier = $identifier;
    }

    /**
     * Helper method to create card from array
     *
     * @param array $cardItem - card detail in array format
     *
     * @return TrainBoardingCard
     */
    public static function fromArray(array $cardItem)
    {
        $missingKeys = array_diff(
            static::$requiredAttributes,
            array_keys($cardItem)
        );
        if (count($missingKeys) > 0) {
            throw new InvalidArgumentException(
                sprintf(
                    "Train boarding card missing keys: [%s]",
                    json_encode($missingKeys)
                )
            );
        }
        return new self(
            new Point($cardItem['departure']),
            new Point($cardItem['arrival']),
            $cardItem['seat'],
            $cardItem['identifier']
        );
    }

    /**
     * Magic method to support treating the object as string
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Take train %s from %s to %s. Seat : %s.',
            $this->identifier,
            (string) $this->departurePoint,
            (string) $this->arrivalPoint,
            $this->seat
        );
    }

    /**
     * Serializes the card to an array that can be serialized
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'transport_type' => TransportType::TRAIN,
            'identifier' => $this->identifier,
            'departure' => (string) $this->departurePoint,
            'arrival' => (string) $this->arrivalPoint,
            'seat' => $this->seat
        ];
    }
}
